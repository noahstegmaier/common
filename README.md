# common

This is the shared code for my (mostly private) c++ projects and very little code (for now)
This is meant to be used as a git submodule and with cmake, but you can probably just copy the files you need.

## Contributing
Not open to contributions, but feel free to ask questions.

## License
This is mostly boilerplate code and nothing important so it's all in Public Domain
