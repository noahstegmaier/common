#include "fshelpers.hpp"

bool readWholeFile(const fs::path &filepath, std::vector<u8> &content) {
    // TODO: check content size and append
    std::basic_ifstream<char> file(filepath, std::ios::binary | std::ios::ate);
    if (!file.is_open()) {
        return false;
    }
    size length = file.tellg();
    content.resize(length);
    file.seekg(0);
    file.read(reinterpret_cast<char *>(content.data()), length);
    file.close();
    return true;
}

bool writeWholeFile(const fs::path &filepath, const std::vector<u8> &content) {
    std::basic_ofstream<char> file(filepath, std::ios::binary);
    if (!file.is_open()) {
        return false;
    }
    file.write(reinterpret_cast<const char *>(content.data()), content.size());
    file.close();
    return true;
}
