#include "stringhelpers.hpp"

void tolower(std::string &in) {
    for (auto &e : in) {
        // https://en.cppreference.com/w/cpp/string/byte/tolower
        // not quite sure if the reasoning on cppreference on those casts makes sense
        e = static_cast<char>(std::tolower(static_cast<unsigned char>(e)));
    }
}
