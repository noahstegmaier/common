cmake_minimum_required(VERSION 3.20)

project(noah_common VERSION 0.0.1 LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

file(GLOB_RECURSE sources CONFIGURE_DEPENDS src/*.cpp)
# maybe some private headers in the future
file(GLOB_RECURSE headers2 CONFIGURE_DEPENDS src/*.hpp)
file(GLOB_RECURSE headers CONFIGURE_DEPENDS include/*.hpp)

add_library(common STATIC ${headers} ${headers2} ${sources})
target_include_directories(common PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)
# to be able to include corresponding headers without common/ only in the cpp files
target_include_directories(common PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include/common)
