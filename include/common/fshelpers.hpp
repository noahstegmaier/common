#pragma once
#include <vector>
#include <filesystem>
namespace fs = std::filesystem;
#include <fstream>
#include "saneints.hpp"

// returns false on fail
// reading hte whole file first before doing any processing makes code easier without much
// performance penalty IF you end up allocating more memory than the filessize
bool readWholeFile(const fs::path &filepath, std::vector<u8> &content);
// returns false on fail
bool writeWholeFile(const fs::path &filepath, const std::vector<u8> &content);
