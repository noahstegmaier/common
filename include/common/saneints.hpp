#pragma once
#include <cstdint>
using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;
using i8 = std::uint8_t;
using i16 = std::uint16_t;
using i32 = std::uint32_t;
using i64 = std::uint64_t;
using size = std::size_t;
